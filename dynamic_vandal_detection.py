
import numpy as np
import json
from collections import defaultdict
# import theano.tensor as T
from keras import backend as K
from keras.layers import Input
from keras.models import Sequential, Model
from keras.layers import merge
from keras.layers.recurrent import LSTM
from keras.layers.core import Activation, Dense, Masking, Permute, Reshape
from keras.layers.merge import Concatenate, Dot
from keras.layers.wrappers import TimeDistributed
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import accuracy_score, classification_report
from util import probas_to_classes
from keras.layers import Merge


def prob2Class(predicted, thr):
    tmpPre = list()
    preMark = list()
    preSteps = list()
    detPoints = list()
    final_pre = list()
    for i, usr in enumerate(predicted):
        last_step_prob = usr[-1][-1]
        if last_step_prob >= thr:
            final_pre.append(1)
        else:
            final_pre.append(0)

        for j, timSp in enumerate(usr):
            if j < len(usr) - 1:
                if timSp[-1] >= thr:
                    tmpPre.append(1)
                    preMark.append(1)
                    preSteps.append(len(usr) - 1 - j)
                    detPoints.append(j)
                    break
            else:
                if timSp[-1] > .5:
                    tmpPre.append(1)
                else:
                    tmpPre.append(0)
                preSteps.append(0)
                preMark.append(0)
                detPoints.append(j)
    return np.array(tmpPre), np.array(preMark), np.array(preSteps), np.array(detPoints), np.array(final_pre)



def dynDetRevTtlCgrTrain(X_train_rev, X_train_ttl, X_train_cgr, y_train):
    batch_size = 128
    maxlen = 63
    revFeaDim = 2
    ttlFeaDim = 50
    cgrFeaDim = 50
    hidden_size = 32

    checkpoint = ModelCheckpoint(filepath='best_model.hdf5', save_best_only=True)
    early_stop = EarlyStopping(patience=10)

    revert_input = Input((maxlen, revFeaDim))
    mask_revert = Masking(mask_value=0., input_shape=(maxlen, revFeaDim))(revert_input)
    revert_lstm = LSTM(hidden_size, return_sequences=False, name='revert_lstm')(mask_revert)

    title_input = Input((maxlen, ttlFeaDim))
    mask_title = Masking(mask_value=0., input_shape=(maxlen, ttlFeaDim))(title_input)
    title_lstm = LSTM(hidden_size, return_sequences=False, name='title_lstm')(mask_title)

    category_input = Input((maxlen, cgrFeaDim))
    mask_cgr = Masking(mask_value=0., input_shape=(maxlen, cgrFeaDim))(category_input)
    category_lstm = LSTM(hidden_size, return_sequences=False, name='time_lstm')(mask_cgr)

    concat_merge = merge([revert_lstm, title_lstm, category_lstm], mode=lambda x: K.stack([x[0], x[1], x[2]], axis=1),
                         output_shape=(3, hidden_size))
    tanh_merge = Dense(units=hidden_size, activation='tanh', name='tanh_merge_layer')(concat_merge)
    pre_attention = Dense(units=1, name='pre_att_layer')(tanh_merge)
    reshape_att = Permute((2, 1))(pre_attention)
    attention = Activation('softmax', name='att_layer')(reshape_att)
    # merged = Reshape((hidden_size,))(merge([attention, concat_merge], mode='dot', dot_axes=(2, 1)))
    merged = Dot(axes=(2, 1))([attention, concat_merge])
    merged = Reshape((hidden_size,))(merged)
    output = Dense(units=2, activation="softmax", name='output_layer')(merged)
    detect_model = Model(inputs=[revert_input, title_input, category_input], outputs=output)
    detect_model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    detect_model.fit([X_train_rev, X_train_ttl, X_train_cgr], y_train, batch_size=batch_size, epochs=30,
                     validation_split=0.1, callbacks=[checkpoint, early_stop], verbose=2)

    detect_model.load_weights('best_model.hdf5')


    weights = defaultdict(list)
    for i, layer in enumerate(detect_model.layers):
        if layer.name == 'title_lstm':
            weights['title_lstm'] = layer.get_weights()
        elif layer.name == 'revert_lstm':
            weights['revert_lstm'] = layer.get_weights()
        elif layer.name == 'time_lstm':
            weights['time_lstm'] = layer.get_weights()
        elif layer.name == 'tanh_merge_layer':
            weights['tanh_merge_layer'] = layer.get_weights()
        elif layer.name == 'pre_att_layer':
            weights['pre_att_layer'] = layer.get_weights()
        elif layer.name == 'output_layer':
            weights['output_layer'] = layer.get_weights()

    return weights



def dynDetRevTtlCgr(X_train_rev, X_train_ttl, X_train_cgr, X_test_rev, X_test_ttl, X_test_cgr, y_train, y_test,
                    valTestSeqLen, f_data):
    maxlen = 63
    ttlFeaDim = 50
    revFeaDim = 2
    cgrFeaDim = 50
    hidden_size = 32

    weights = dynDetRevTtlCgrTrain(X_train_rev, X_train_ttl, X_train_cgr, y_train)

    revert_input = Input((maxlen, revFeaDim))
    mask_revert = Masking(mask_value=0., input_shape=(maxlen, revFeaDim))(revert_input)
    revert_lstm = LSTM(hidden_size, return_sequences=True, name='revert_lstm', weights=weights['revert_lstm'])(
        mask_revert)

    title_input = Input((maxlen, ttlFeaDim))
    mask_title = Masking(mask_value=0., input_shape=(maxlen, ttlFeaDim))(title_input)
    title_lstm = LSTM(hidden_size, return_sequences=True, name='title_lstm', weights=weights['title_lstm'])(mask_title)

    category_input = Input((maxlen, cgrFeaDim))
    mask_cgr = Masking(mask_value=0., input_shape=(maxlen, cgrFeaDim))(category_input)
    cgr_lstm = LSTM(hidden_size, return_sequences=True, name='time_lstm', weights=weights['time_lstm'])(mask_cgr)

    concat_merge = merge([revert_lstm, title_lstm, cgr_lstm], mode=lambda x: K.stack([x[0], x[1], x[2]], axis=2),
                         output_shape=(maxlen, 3, hidden_size))
    merge_input = Input((3, hidden_size))
    tanh_merge = Dense(units=hidden_size, activation='tanh', weights=weights['tanh_merge_layer'],
                       name='tanh_merge_layer')(merge_input)
    pre_attention = Dense(units=1, weights=weights['pre_att_layer'], name='pre_att_layer')(tanh_merge)
    pre_attention = Permute((2, 1))(pre_attention)
    attention = Activation('softmax', name='att_layer')(pre_attention)
    # merged = Reshape((hidden_size,))(merge([attention, merge_input], mode='dot', dot_axes=(2, 1)))
    merged = Dot(axes=(2, 1))([attention, merge_input])
    merged = Reshape((hidden_size,))(merged)
    output = Dense(units=2, activation="softmax", name='output_layer', weights=weights['output_layer'])(merged)
    predict_model = Model(inputs=merge_input, outputs=output)

    seq_output = TimeDistributed(predict_model, input_shape=(maxlen, 3, hidden_size))(concat_merge)
    detect_model = Model(inputs=[revert_input, title_input, category_input], outputs=seq_output)
    detect_model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    #
    predited = detect_model.predict([X_test_rev, X_test_ttl, X_test_cgr])
    predited = [predited[i, maxlen - val:maxlen, :] for i, val in enumerate(valTestSeqLen)]

    len_predicted = [len(p) for p in predited]
    long_len_idx = np.where(np.array(len_predicted) > 1)

    thrlds = [.5, .6, .7, .8, .9]
    for thrld in thrlds:
        pr, preMark, preSteps, detPoints, final_pre = prob2Class(predited, thrld)

        preInd = (preMark == 1)

        preSteps = [ss for yy, pp, ss, dd in zip(y_test, pr, preSteps, predited) if
                    yy == pp and pp == 1 and len(dd) > 1]

        percent_of_early = len([ss for yy, pp, ss, dd in zip(y_test, pr, preMark, predited) if
                                yy == pp and ss == 1 and yy == 1 and len(dd) > 1]) / \
                           float(len([ss for yy, pp, ss, dd in zip(y_test, pr, preMark, predited) if
                                      yy == 1 and len(dd) > 1]))

        precision = np.sum(y_test[preInd] == pr[preInd]) / float(y_test[preInd].shape[0])
        recall = np.sum(y_test[preInd] == pr[preInd]) / float(np.sum([y_test[long_len_idx] == 1]))
        f1 = 2 * (precision * recall) / (precision + recall)

        print("threshold: " + str(thrld))
        # print(accuracy_score(y_test, final_pre))
        print("vandal early detection")
        print(classification_report(y_test[long_len_idx], pr[long_len_idx], digits=4))
        print("vandal detection")
        print(classification_report(y_test, final_pre, digits=4))

        print("number of edits: " + str(np.mean(preSteps)))
        print("ratio of early detected: " + str(percent_of_early))
        print("===============================")
