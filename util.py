import os
import glob
import json
import datetime
# from datetime import *
import numpy as np
import pandas as pd
from shutil import rmtree
from collections import defaultdict
from keras.preprocessing.sequence import pad_sequences

f_pages = 'vews_dataset_v1.1\\pages.tsv'
f_users = 'vews_dataset_v1.1\\users.tsv'
files = glob.glob("vews_dataset_v1.1\\*.csv")
filesB = glob.glob("vews_dataset_v1.1\\benign_*.csv")
filesV = glob.glob("vews_dataset_v1.1\\vandal_*.csv")

def usrEncode(x,n):
    x = int(x)
    result = np.zeros(n)
    result[x] = 1
    return result

def probas_to_classes(y_pred, p=0.5):
    final_pre = []
    for i, usr in enumerate(y_pred):
        last_step_prob = usr[-1]
        if last_step_prob >= p:
            final_pre.append(1)
        else:
            final_pre.append(0)
    return np.array(final_pre)
    # if len(y_pred.shape) > 1 and y_pred.shape[1] > 1:
    #     return np.argmax(y_pred, axis=1)
    # return np.array([1 if p_y > p else 0 for p_y in y_pred])


def getUserDict(f_users):
    users_frame = pd.read_csv(f_users, sep='\t', header=None)
    users_frame = users_frame.applymap(str)
    user2id = users_frame.set_index(0)[1].to_dict()

    uid2type = users_frame.set_index(1)[4].to_dict()
    id2user = users_frame.set_index(1)[0].to_dict()
    return user2id, uid2type, id2user


def getPageDict(f_pages):
    pages_frame = pd.read_csv(f_pages, sep=',')
    pages_frame = pages_frame.applymap(str)
    page2id = pages_frame.set_index('pagetitle')['pageid'].to_dict()
    id2Cgr = pages_frame.set_index('pageid')['pagecategories'].to_dict()
    id2page = pages_frame.set_index('pageid')['pagetitle'].to_dict()
    return page2id, id2Cgr, id2page


def TleRevTim(files, user2id, page2id):
    titleSet = defaultdict(list)
    revSet = defaultdict(list)
    timSet = defaultdict(list)

    for f in files:
        df = pd.read_csv(f, sep=',')
        for index, row in df.iterrows():
            try:
                usrid = user2id[row['username']]
            except Exception as e:
                print(row['username'])
            try:
                pageid = page2id[row['pagetitle']]
            except Exception as e:
                print(row['pagetitle'])

            title = row['pagetitle'].lower()
            if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
                continue

            isRev = row['isReverted']
            revTime = row['revtime']
            titleSet[usrid].append(pageid)
            if isRev:
                revSet[usrid].append(1)
            else:
                revSet[usrid].append(2)
            timSet[usrid].append(revTime)

    for usrid in titleSet:

        collTle = list()
        collRev = list()
        collTim = list()
        titles = titleSet[usrid]
        Revs = revSet[usrid]
        Tims = timSet[usrid]

        for i, title in enumerate(titles):
            if i == len(titles) - 1:
                collTle.append(title)
                collRev.append(Revs[i])
                collTim.append(Tims[i])
                continue
            else:
                if title != titles[i + 1]:
                    collTle.append(title)
                    collRev.append(Revs[i])
                    collTim.append(Tims[i])

        titleSet[usrid] = collTle
        revSet[usrid] = collRev
        timSet[usrid] = collTim

    return titleSet, revSet, timSet


def editTimeIval(editTime):
    editRec = json.loads(editTime)
    for usrid in editRec:
        timeList = editRec[usrid]
        for ind, t in enumerate(timeList):
            y_m = datetime.datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ')
            y_m = y_m.strftime("%Y-%m")
            timeList[ind] = y_m
        editRec[usrid] = timeList

    # editRecC = json.dumps(editRec)
    timeDict = {
        '2013-01': 1,
        '2013-02': 2,
        '2013-03': 3,
        '2013-04': 4,
        '2013-05': 5,
        '2013-06': 6,
        '2013-07': 7,
        '2013-08': 8,
        '2013-09': 9,
        '2013-10': 10,
        '2013-11': 11,
        '2013-12': 12,
        '2014-01': 13,
        '2014-02': 14,
        '2014-03': 15,
        '2014-04': 16,
        '2014-05': 17,
        '2014-06': 18,
        '2014-07': 19}

    for usrid in editRec:
        tmp = editRec[usrid]
        for i, t in enumerate(tmp):
            if '2013-01' > t or t > '2014-07':
                tmp[i] = 20
            else:
                tmp[i] = timeDict[t]
        # editRec[usrid] = etEncode(tmp, 21)    encode with 0,1
        editRec[usrid] = tmp

    return editRec


def getLabel(filesB,filesV,user2id,posLabel,negLabel):
    usrListB = list()
    usrListV = list()
    userid2Label = dict()
    for f in filesB:
        df = pd.read_csv(f,sep=',')
        for index, row in df.iterrows():
            usrid = user2id[row['username']]
            usrListB.append(usrid)
    usrListB = list(set(usrListB))
    for val in usrListB:
        userid2Label[val] = posLabel


    for f in filesV:
        df = pd.read_csv(f,sep=',')
        for index, row in df.iterrows():
            usrid = user2id[row['username']]
            usrListV.append(usrid)
    usrListV = list(set(usrListV))
    for val in usrListV:
        userid2Label[val] = negLabel


    return userid2Label

def encode(x, n):
    x = int(x)
    result = np.zeros(n)
    result[x-1] = 1
    return result


def train_test_split(dataSet1, dataSet2, dataSet3, dataSet4, labels, masks, usrs, pages, trainRatio, testRatio):
    if trainRatio + testRatio != 1.0:
        raise Exception("trainRatio + testRatio != 1.0")

    size = dataSet1.shape[0]
    thre = int(trainRatio * 10)

    indSet = np.random.randint(0,10, size) + 1
    trainInd = [ i for i, val in enumerate(indSet) if val <= thre]
    testInd = [ i for i, val in enumerate(indSet) if val > thre]

    trainSet1 = [ dataSet1[i] for i in trainInd ]
    trainSet2 = [ dataSet2[i] for i in trainInd ]
    trainSet3 = [ dataSet3[i] for i in trainInd ]
    trainSet4 = [ dataSet4[i] for i in trainInd ]
    trainlabel = [ labels[i] for i in trainInd ]
    trainMasks = [ masks[i] for i in trainInd ]

    trainUsrs = [ usrs[i] for i in trainInd ]
    trainPages = [ pages[i] for i in trainInd ]


    testSet1 = [ dataSet1[i] for i in testInd ]
    testSet2 = [ dataSet2[i] for i in testInd ]
    testSet3 = [ dataSet3[i] for i in testInd ]
    testSet4 = [ dataSet4[i] for i in testInd ]
    testlabel = [ labels[i] for i in testInd ]
    testMasks = [ masks[i] for i in testInd ]


    testUsrs = [ usrs[i] for i in testInd ]
    testPages = [ pages[i] for i in testInd ]

    return np.array(trainSet1), np.array(testSet1), np.array(trainSet2), np.array(testSet2), np.array(trainSet3), np.array(testSet3), np.array(trainSet4), np.array(testSet4), np.array(trainlabel), np.array(testlabel), np.array(trainMasks), np.array(testMasks), np.array(trainUsrs), np.array(testUsrs), np.array(trainPages), np.array(testPages)


def getDataSet(revSet, titleSet, title2Vec, cgr2Vec, user2Label, seqDwTrld, seqUpTrld):

    X_rev, X_ttl, X_cgr, X_usrs, labels, X_pages = getSrcDataSeq(revSet, titleSet, user2Label, title2Vec, cgr2Vec)

    X_rev = [rev for x, rev in zip(X_ttl,X_rev) if seqDwTrld<= len(x) <= seqUpTrld]
    X_cgr = [cgr for x, cgr in zip(X_ttl,X_cgr) if seqDwTrld<= len(x) <= seqUpTrld]

    X_usrs = [usr for x, usr in zip(X_ttl,X_usrs) if seqDwTrld<= len(x) <= seqUpTrld]
    labels = [lbl for x, lbl in zip(X_ttl,labels) if seqDwTrld<= len(x) <= seqUpTrld]
    X_pages = [page for x, page in zip(X_ttl,X_pages) if seqDwTrld<= len(x) <= seqUpTrld]

    X_ttl = [x for x in X_ttl if seqDwTrld<= len(x) <= seqUpTrld]

    maxlen = max([len(x) for x in X_ttl])
    editSeqLen = [len(x) for x in X_ttl]

    X_rev = pad_sequences(X_rev, maxlen=maxlen, dtype='float')
    X_ttl = pad_sequences(X_ttl, maxlen=maxlen, dtype='float')
    X_cgr = pad_sequences(X_cgr, maxlen=maxlen, dtype='float')
    X_usrs = np.array(X_usrs)
    labels = np.array(labels)
    X_pages = np.array(X_pages)
    editSeqLen = np.array(editSeqLen)

    return X_rev, X_ttl, X_cgr, X_usrs, labels, X_pages, editSeqLen


def getSrcDataSeq(revSet, titleSet, user2Label, title2Vec, cgr2Vec):
    X_rev = list()
    X_ttl = list()
    X_cgr = list()
    X_usrs = list()
    labels = list()
    X_pages = list()

    for usrid in titleSet:

        X_usrs.append(usrid)
        labels.append(user2Label[usrid])

        spl_rev = list()
        spl_ttl = list()
        spl_cgr = list()

        Revs = revSet[usrid]
        titles = titleSet[usrid]

        for i, pageid in enumerate(titles):
            spl_ttl.append(title2Vec[pageid])
            spl_cgr.append(cgr2Vec[pageid])
            spl_rev.append(Revs[i])

        X_pages.append(titles)
        X_rev.append(spl_rev)
        X_ttl.append(spl_ttl)
        X_cgr.append(spl_cgr)

    return X_rev, X_ttl, X_cgr, X_usrs, labels, X_pages


def editSeqProcess(revSet, titleSet, timSet):
    for usrid in titleSet:

        collRev = list()
        collTle = list()
        collTim = list()

        revs = revSet[usrid]
        titles = titleSet[usrid]
        tims = timSet[usrid]

        for i, title in enumerate(titles):
            if i == len(titles) - 1:
                collRev.append(revs[i])
                collTle.append(title)
                collTim.append(tims[i])
                continue
            else:
                if title != titles[i + 1]:
                    collRev.append(revs[i])
                    collTle.append(title)
                    collTim.append(tims[i])

        revSet[usrid] = collRev
        titleSet[usrid] = collTle
        timSet[usrid] = collTim

    return revSet, titleSet, timSet


def RevTtlCgr(files, user2id, page2id):
    revSetTra = defaultdict(list)
    ttlSetTra = defaultdict(list)

    revSetTes = defaultdict(list)
    ttlSetTes = defaultdict(list)

    timSetTra = defaultdict(list)
    timSetTes = defaultdict(list)

    boundTime = datetime.datetime.strptime("2013-10-01T00:00:00Z", '%Y-%m-%dT%XZ')

    for i, f in enumerate(files):
        df = pd.read_csv(f, sep=',')
        for index, row in df.iterrows():
            try:
                usrid = user2id[row['username']]
            except Exception as e:
                print(row['username'])
            try:
                pageid = page2id[row['pagetitle']]
            except Exception as e:
                print(row['pagetitle'])

            title = row['pagetitle'].lower()
            if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
                continue

            reviseTime = row['revtime']
            reviseTime = datetime.datetime.strptime(reviseTime, '%Y-%m-%dT%XZ')

            isRev = row['isReverted']
            if isRev:
                revertTime = row['revertTime']
                revertTime = datetime.datetime.strptime(revertTime, '%Y-%m-%dT%XZ')
                td = revertTime - reviseTime
                if td.days * 24 + td.seconds / (60 * 60) >= 12:
                    continue

            if i < 9 or 18 < i < 28:

                if reviseTime >= boundTime:
                    continue
                if isRev:
                    revSetTra[usrid].append(1)
                else:
                    revSetTra[usrid].append(2)
                ttlSetTra[usrid].append(pageid)
                timSetTra[usrid].append(row['revtime'])

            else:

                if reviseTime < boundTime:
                    continue
                if isRev:
                    revSetTes[usrid].append(1)
                else:
                    revSetTes[usrid].append(2)
                ttlSetTes[usrid].append(pageid)
                timSetTes[usrid].append(row['revtime'])

    revSetTra, ttlSetTra, timSetTra = editSeqProcess(revSetTra, ttlSetTra, timSetTra)
    revSetTes, ttlSetTes, timSetTes = editSeqProcess(revSetTes, ttlSetTes, timSetTes)

    return revSetTra, ttlSetTra, revSetTes, ttlSetTes, timSetTra, timSetTes



def getSamples(seqDwTrld, seqUpTrld, splRatio, f_title2Vec, f_cgr2Vec, f_data):
    directory = f_data + "sv" + str(seqDwTrld) + "_" + str(seqUpTrld) + "_" + str(splRatio)
    if os.path.exists(directory):
        rmtree(directory)
    os.makedirs(directory)

    directory += "\\"

    # load user-page information from dictionary file and raw data.
    user2id, uid2type, id2user = getUserDict(f_users)
    page2id, page2Cgr, id2page = getPageDict(f_pages)
    titleSet, revSet, timSet = TleRevTim(files, user2id, page2id)

    # store the original edit-time for pages
    f = open(directory + "editSeqTimDict.json", "w+")
    f.write(json.dumps(timSet))
    f.close()

    timSet = editTimeIval(json.dumps(timSet))

    for usrid in revSet:
        tmpRevList = revSet[usrid]
        for i, x in enumerate(tmpRevList):
            tmpRevList[i] = encode(x, 2)
        revSet[usrid] = tmpRevList

    # revSet = {usrid: map(lambda x: encode(x,3), revSet[usrid]) for usrid in revSet}

    # title2Vec = json.loads(open(f_titleVec, "r").readline())
    title2Vec = json.loads(open(f_title2Vec, "r").readline())
    cgr2Vec = json.loads(open(f_cgr2Vec, "r").readline())
    user2Label = getLabel(filesB, filesV, user2id, 0, 1)

    X_tle = list()
    X_cgr = list()
    X_rev = list()
    X_tim = list()
    y = list()

    X_usrs = list()
    X_pages = list()

    for usrid in titleSet:
        X_usrs.append(usrid)
        y.append(user2Label[usrid])

        spl_tle = list()
        spl_cgr = list()
        spl_rev = list()
        spl_tim = list()

        titles = titleSet[usrid]
        Revs = revSet[usrid]
        Tims = timSet[usrid]

        for i, pageid in enumerate(titles):
            spl_tle.append(title2Vec[pageid])
            spl_cgr.append(cgr2Vec[pageid])
            spl_rev.append(Revs[i])
            spl_tim.append(Tims[i])

        X_pages.append(titles)
        X_cgr.append(spl_cgr)
        X_tle.append(spl_tle)
        X_rev.append(spl_rev)
        X_tim.append(spl_tim)

    X_usrs = [usr for x, usr in zip(X_tle, X_usrs) if seqDwTrld <= len(x) <= seqUpTrld]
    y = [lbl for x, lbl in zip(X_tle, y) if seqDwTrld <= len(x) <= seqUpTrld]
    X_pages = [page for x, page in zip(X_tle, X_pages) if seqDwTrld <= len(x) <= seqUpTrld]
    X_rev = [rev for x, rev in zip(X_tle, X_rev) if seqDwTrld <= len(x) <= seqUpTrld]
    X_tim = [tim for x, tim in zip(X_tle, X_tim) if seqDwTrld <= len(x) <= seqUpTrld]
    X_cgr = [cgr for x, cgr in zip(X_tle, X_cgr) if seqDwTrld <= len(x) <= seqUpTrld]
    X_tle = [x for x in X_tle if seqDwTrld <= len(x) <= seqUpTrld]

    maxlen = max([len(x) for x in X_tle])
    editSeqLen = [len(x) for x in X_tle]

    X_tle = pad_sequences(X_tle, maxlen=maxlen, dtype='float')
    X_cgr = pad_sequences(X_cgr, maxlen=maxlen, dtype='float')
    X_rev = pad_sequences(X_rev, maxlen=maxlen, dtype='float')
    X_tim = pad_sequences(X_tim, maxlen=maxlen, dtype='float')
    y = np.array(y)

    X_train_ttl, X_test_ttl, X_train_rev, X_test_rev, X_train_tim, X_test_tim, X_train_cgr, X_test_cgr,\
    y_train, y_test, valTrainSeqLen, valTestSeqLen, X_train_usr, X_test_usr, X_train_page, X_test_page = \
        train_test_split(X_tle, X_rev, X_tim, X_cgr, y, editSeqLen, X_usrs, X_pages, splRatio, 1 - splRatio)

    print(X_train_cgr.shape, X_test_cgr.shape, X_cgr.shape)

    X_train_tim = np.reshape(X_train_tim, (X_train_tim.shape[0], X_train_tim.shape[1], 1))
    X_test_tim = np.reshape(X_test_tim, (X_test_tim.shape[0], X_test_tim.shape[1], 1))
    X_tim = np.reshape(X_tim, (X_tim.shape[0], X_tim.shape[1], 1))

    np.save(directory + "X_ttl.npy", X_tle)
    np.save(directory + "X_cgr.npy", X_cgr)
    np.save(directory + "X_rev.npy", X_rev)
    np.save(directory + "X_tim.npy", X_tim)
    np.save(directory + "y.npy", y)

    np.save(directory + "X_train_ttl.npy", X_train_ttl)
    np.save(directory + "X_test_ttl.npy", X_test_ttl)

    np.save(directory + "X_train_cgr.npy", X_train_cgr)
    np.save(directory + "X_test_cgr.npy", X_test_cgr)

    np.save(directory + "X_train_rev.npy", X_train_rev)
    np.save(directory + "X_test_rev.npy", X_test_rev)

    np.save(directory + "X_train_tim.npy", X_train_tim)
    np.save(directory + "X_test_tim.npy", X_test_tim)

    np.save(directory + "y_train.npy", y_train)
    np.save(directory + "y_test.npy", y_test)

    np.save(directory + "valTrainSeqLen.npy", valTrainSeqLen)
    np.save(directory + "valTestSeqLen.npy", valTestSeqLen)

    np.save(directory + "X_train_usr.npy", X_train_usr)
    np.save(directory + "X_train_page.npy", X_train_page)

    np.save(directory + "X_test_usr.npy", X_test_usr)
    np.save(directory + "X_test_page.npy", X_test_page)

    np.save(directory + "X_pages.npy", X_pages)
    np.save(directory + "X_usrs.npy", X_usrs)
    np.save(directory + "valSeqLen.npy", editSeqLen)


def getSamplesForEarDet(seqDwTrld, seqUpTrld, f_title2Vec, f_cgr2Vec, f_data):
    directory = f_data + "sv" + str(seqDwTrld) + "_" + str(seqUpTrld)
    if os.path.exists(directory):
        rmtree(directory)
    os.makedirs(directory)

    directory += "\\"

    # load user-page information from dictionary file and raw data.
    user2id, uid2type, id2user = getUserDict(f_users)
    page2id, page2Cgr, id2page = getPageDict(f_pages)
    revSetTra, ttlSetTra, revSetTes, ttlSetTes, timSetTra, timSetTes = RevTtlCgr(files, user2id, page2id)

    # store the original edit-time for pages
    f = open(directory + "editSeqTimDict.json", "w+")
    timSetTes.update(timSetTra)
    f.write(json.dumps(timSetTes))
    f.close()


    revSetTra = {usrid: list(map(lambda x: encode(x, 2).tolist(), revertList)) for usrid, revertList in revSetTra.items()}
    revSetTes = {usrid: list(map(lambda x: encode(x, 2).tolist(), revertList)) for usrid, revertList in revSetTes.items()}

    title2Vec = json.loads(open(f_title2Vec, "r").readline())
    cgr2Vec = json.loads(open(f_cgr2Vec, "r").readline())
    user2Label = getLabel(filesB, filesV, user2id, 0, 1)

    X_train_rev, X_train_ttl, X_train_cgr, X_train_usr, y_train, X_train_page, valTrainSeqLen = getDataSet(revSetTra,
                                                                                                           ttlSetTra,
                                                                                                           title2Vec,
                                                                                                           cgr2Vec,
                                                                                                           user2Label,
                                                                                                           seqDwTrld,
                                                                                                           seqUpTrld)
    X_test_rev, X_test_ttl, X_test_cgr, X_test_usr, y_test, X_test_page, valTestSeqLen = getDataSet(revSetTes,
                                                                                                    ttlSetTes,
                                                                                                    title2Vec, cgr2Vec,
                                                                                                    user2Label,
                                                                                                    seqDwTrld,
                                                                                                    seqUpTrld)

    X_rev = np.concatenate((X_train_rev, X_test_rev), axis=0)
    X_ttl = np.concatenate((X_train_ttl, X_test_ttl), axis=0)
    X_cgr = np.concatenate((X_train_cgr, X_test_cgr), axis=0)
    X_usr = np.concatenate((X_train_usr, X_test_usr), axis=0)
    y = np.concatenate((y_train, y_test), axis=0)
    X_page = np.concatenate((X_train_page, X_test_page), axis=0)
    valSeqLen = np.concatenate((valTrainSeqLen, valTestSeqLen), axis=0)

    np.save(directory + "X_rev.npy", X_rev)
    np.save(directory + "X_ttl.npy", X_ttl)
    np.save(directory + "X_cgr.npy", X_cgr)
    np.save(directory + "X_usr.npy", X_usr)
    np.save(directory + "X_page.npy", X_page)
    np.save(directory + "y.npy", y)
    np.save(directory + "valSeqLen.npy", valSeqLen)

    np.save(directory + "X_train_rev.npy", X_train_rev)
    np.save(directory + "X_test_rev.npy", X_test_rev)

    np.save(directory + "X_train_ttl.npy", X_train_ttl)
    np.save(directory + "X_test_ttl.npy", X_test_ttl)

    np.save(directory + "X_train_cgr.npy", X_train_cgr)
    np.save(directory + "X_test_cgr.npy", X_test_cgr)

    np.save(directory + "X_train_usr.npy", X_train_usr)
    np.save(directory + "X_test_usr.npy", X_test_usr)

    np.save(directory + "y_train.npy", y_train)
    np.save(directory + "y_test.npy", y_test)

    np.save(directory + "X_train_page.npy", X_train_page)
    np.save(directory + "X_test_page.npy", X_test_page)

    np.save(directory + "valTrainSeqLen.npy", valTrainSeqLen)
    np.save(directory + "valTestSeqLen.npy", valTestSeqLen)