# README #

### The code for paper "Wikipedia Vandal (Early) Detection: from User Behavior to User Embedding" ###


### Set up ###

* Download the UMD Wikipedia dataset from [here](https://www.cs.umd.edu/~vs/vews/)
* Unzip the dataset and put all the files into **dataset** folder
* Down the Glove word embeddings from [here](http://nlp.stanford.edu/data/glove.twitter.27B.zip)
* Unzip the file and put the 'glove.twitter.27B.50d.txt' file into **dataset** folder
* Run `python preprocess.py` to preprocess the dataset and generate two files about the title embeddings and category embeddings
* Run `python run.py`

### Requirements ###

* Python 3.6
* Keras 2.0 +