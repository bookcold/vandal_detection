# -*- coding: utf-8 -*-
import numpy as np
import glob
from dynamic_vandal_detection import dynDetRevTtlCgr, detRevTtlCat, detRev
from util import getSamplesForEarDet, usrEncode, getSamples


# from dynDetUtiAutoWeights import dynDetRevTtl, dynDetRevTtlCgr



f_pages = 'dataset\\pages.tsv'
f_users = 'dataset\\users.tsv'
files = glob.glob("dataset\\*.csv")
filesB = glob.glob("dataset\\benign_*.csv")
filesV = glob.glob("dataset\\vandal_*.csv")
f_title2Vec = "dataset\\title2Vec.json"
f_cgr2Vec = 'dataset\\cgr2Vec.json'
storePath = "dataset\\temp\\"



getSamplesForEarDet(1, 63, f_title2Vec, f_cgr2Vec, storePath)
#
# exit(0)


f_data = storePath + "sv1_63\\"

X_rev = np.load(f_data + "X_rev.npy")
X_ttl = np.load(f_data + "X_ttl.npy")
X_cgr = np.load(f_data + "X_cgr.npy")
X_usr = np.load(f_data + "X_usr.npy")
y = np.load(f_data + "y.npy")
X_page = np.load(f_data + "X_page.npy")
valSeqLen = np.load(f_data + "valSeqLen.npy")

X_train_rev = np.load(f_data + "X_train_rev.npy")
X_test_rev = np.load(f_data + "X_test_rev.npy")

X_train_ttl = np.load(f_data + "X_train_ttl.npy")
X_test_ttl = np.load(f_data + "X_test_ttl.npy")

X_train_cgr = np.load(f_data + "X_train_cgr.npy")
X_test_cgr = np.load(f_data + "X_test_cgr.npy")

X_train_usr = np.load(f_data + "X_train_usr.npy")
X_test_usr = np.load(f_data + "X_test_usr.npy")

y_train = np.load(f_data + "y_train.npy")
y_test = np.load(f_data + "y_test.npy")

X_train_page = np.load(f_data + "X_train_page.npy")
X_test_page = np.load(f_data + "X_test_page.npy")

valTrainSeqLen = np.load(f_data + "valTrainSeqLen.npy")
valTestSeqLen = np.load(f_data + "valTestSeqLen.npy")



print("# of users: ", y.shape[0])
print("size of training set: ", y_train.shape[0])
print("size of testing set: ", y_test.shape[0])


print("# of vandals: ", np.sum(y == 1))
print("# of benign users: ", np.sum(y == 0))

y_train = list(map(lambda x: usrEncode(x,2), y_train))
y_train = np.array(y_train)

dynDetRevTtlCgr(X_train_rev, X_train_ttl, X_train_cgr, X_test_rev,
                   X_test_ttl, X_test_cgr, y_train, y_test, valTestSeqLen, f_data)