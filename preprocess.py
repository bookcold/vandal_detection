# -*- coding: utf-8 -*-
import os
import numpy as np
import glob
import json
from gensim.models import KeyedVectors
import pandas as pd
from collections import defaultdict
import string



def getEmbVec(titleList, id2page, id2Cgr, f_gloves, f_title2Vec, f_cgr2Vec):
    model = KeyedVectors.load_word2vec_format(f_gloves, binary=False)
    mean, cov = getDistri(f_gloves)

    modelT, modelDefT = getWordEmbDictTle(model, titleList, id2page, mean, cov)
    modelC, modelDefC = getWordEmbDictCgr(model, titleList, id2Cgr, mean, cov)

    modelT.update(modelC)
    modelT.update(modelDefT)
    modelT.update(modelDefC)

    model = modelT
    title2Vec = getTitleEmbDict(model, titleList, id2page)
    cgr2Vec = getCgrEmbDict(model, titleList, id2Cgr, id2page)

    with open(f_title2Vec, "w") as f_title, open(f_cgr2Vec, "w") as f_cgr:
        f_title.write(json.dumps(title2Vec))
        f_cgr.write(json.dumps(cgr2Vec))




def getWordEmbDictTle(model, pageColl, id2page, mean, cov):
    modelTwi = defaultdict(list)
    modelDef = defaultdict(list)

    for pageid in pageColl:

        title = id2page[pageid].lower()

        if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
            continue
        translator = str.maketrans('', '', string.punctuation)
        title = title.translate(translator)
        title = title.strip().split(" ")

        for w in title:
            try:
                vec = model[w]
                modelTwi.setdefault(w, vec.tolist())
            except KeyError:
                vec = np.random.multivariate_normal(mean, cov).tolist()
                modelDef.setdefault(w, vec)

    return modelTwi, modelDef


def getWordEmbDictCgr(model, pageColl, id2Cgr, mean, cov):
    modelTwi = defaultdict(list)
    modelDef = defaultdict(list)

    for pageid in pageColl:

        cgrList = cgrParse(id2Cgr[pageid])
        if cgrList:
            for cgr in cgrList:
                cgr = cgr.lower()
                translator = str.maketrans('', '', string.punctuation)
                cgr = cgr.translate(translator)
                cgr = cgr.strip().split(" ")

                for w in cgr:
                    try:
                        vec = model[w]
                        modelTwi.setdefault(w, vec.tolist())
                    except KeyError:
                        vec = np.random.multivariate_normal(mean, cov).tolist()
                        modelDef.setdefault(w, vec)

    return modelTwi, modelDef


def getTitleEmbDict(model, pageColl, id2page):
    title2Vec = defaultdict(list)

    for pageid in pageColl:
        wordVec = []
        title = id2page[pageid].lower()

        if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
            continue
        translator = str.maketrans('', '', string.punctuation)
        title = title.translate(translator)
        title = title.strip().split(" ")

        for w in title:
            try:
                vec = model[w]
            except Exception:
                print("word %s" % w + "can not be found in the wordembedding dictionary.")
            wordVec.append(vec)

        wordVec = np.array(wordVec)
        pageVec = np.mean(wordVec, axis=0).tolist()
        title2Vec[pageid] = pageVec
    return title2Vec


def getCgrEmbDict(model, pageColl, id2Cgr, id2page):
    cgr2Vec = defaultdict(list)

    for pageid in pageColl:
        cgrList = cgrParse(id2Cgr[pageid])
        wordVec = []
        if cgrList:
            if len(cgrList) > 5:
                cgrList = cgrList[0:5]
            for cgr in cgrList:
                cgr = cgr.lower()
                translator = str.maketrans('', '', string.punctuation)
                cgr = cgr.translate(translator)
                cgr = cgr.strip().split(" ")

                for w in cgr:
                    try:
                        vec = model[w]
                    except Exception:
                        print("word %s" % w + "can not be found in the wordembedding dictionary.")
                    wordVec.append(vec)
        else:
            title = id2page[pageid].lower()
            if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
                wordVec = np.zeros(50).reshape(1, 50)
            else:
                wordVec = np.ones(50).reshape(1, 50)

        wordVec = np.array(wordVec)
        cgrVec = np.mean(wordVec, axis=0).tolist()
        cgr2Vec[pageid] = cgrVec

    return cgr2Vec


def TleRevTim(files, user2id, page2id):
    titleSet = defaultdict(list)
    revSet = defaultdict(list)
    timSet = defaultdict(list)

    for f in files:
        df = pd.read_csv(f, sep=',')
        for index, row in df.iterrows():
            try:
                usrid = user2id[row['username']]
            except Exception as e:
                print(row['username'])

            try:
                pageid = page2id[row['pagetitle']]
            except Exception as e:
                print(row['pagetitle'])


            title = row['pagetitle'].lower()
            if "user:" in title or "talk:" in title or "user talk:" in title or "wikipedia" in title:
                continue

            isRev = row['isReverted']
            revTime = row['revtime']
            titleSet[usrid].append(pageid)
            if isRev:
                revSet[usrid].append(1)
            else:
                revSet[usrid].append(0)
            timSet[usrid].append(revTime)

    for usrid in titleSet:

        collTle = list()
        collRev = list()
        collTim = list()
        titles = titleSet[usrid]
        Revs = revSet[usrid]
        Tims = timSet[usrid]

        for i, title in enumerate(titles):
            if i == len(titles) - 1:
                collTle.append(title)
                collRev.append(Revs[i])
                collTim.append(Tims[i])
                continue
            else:
                if title != titles[i + 1]:
                    collTle.append(title)
                    collRev.append(Revs[i])
                    collTim.append(Tims[i])

        titleSet[usrid] = collTle
        revSet[usrid] = collRev
        timSet[usrid] = collTim

    return titleSet, revSet, timSet


def getUserDict(f_users):
    users_frame = pd.read_csv(f_users, sep='\t', header=None)
    users_frame = users_frame.applymap(str)
    user2id = users_frame.set_index(0)[1].to_dict()

    uid2type = users_frame.set_index(1)[4].to_dict()
    id2user = users_frame.set_index(1)[0].to_dict()
    return user2id, uid2type, id2user


def getPageDict(f_pages):
    pages_frame = pd.read_csv(f_pages, sep=',')
    pages_frame = pages_frame.applymap(str)
    page2id = pages_frame.set_index('pagetitle')['pageid'].to_dict()
    id2Cgr = pages_frame.set_index('pageid')['pagecategories'].to_dict()
    id2page = pages_frame.set_index('pageid')['pagetitle'].to_dict()
    return page2id, id2Cgr, id2page


def getDistri(f_gloves):
    with open(f_gloves, "r", encoding='utf8') as f:
        f.readline()
        collVec = list()
        for val in f.readlines():
            vec = val.split(" ")[1:]
            vec = [float(i) for i in vec]
            collVec.append(vec)
    collVec = np.array(collVec)

    return np.mean(collVec, axis=0), np.cov(collVec, rowvar=False)


def cgrParse(cgrList):
    crgColl = list()
    for cgr in cgrList.split(":")[1:]:
        cgr = cgr.split("'")
        crgColl.append(cgr[0])
    return crgColl



if __name__ == '__main__':

    # basic data files
    files = glob.glob("dataset\\*.csv")  # Wikipedia raw data
    f_pages = "dataset\\pages.tsv"  # page information
    f_users = "dataset\\users.tsv"  # user information

    # Twitter Glove file
    f_gloves = "dataset\\glove.twitter.27B.50d.txt"

    f_title2Vec = "dataset\\title2Vec.json"  # title2Vec
    f_cgr2Vec = "dataset\\cgr2Vec.json"  # cgr2Vec
    # Note: if # of category is more than 5,
    # the first 5 ones are taken to compute category word-embedding.


    # load data
    user2id, __, __ = getUserDict(f_users)
    page2id, id2Cgr, id2page = getPageDict(f_pages)
    pageSet, __, __ = TleRevTim(files, user2id, page2id)

    # extract pages set
    pages = [pageid for usrid in pageSet for pageid in pageSet[usrid]]
    pages = set(pages)

    # get title/category embedding dictionary files
    getEmbVec(pages, id2page, id2Cgr, f_gloves, f_title2Vec, f_cgr2Vec)
